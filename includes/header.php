<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/clean-blog.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="index.html">CMS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php
                $sql = "SELECT * FROM cms_navigation WHERE parent_id = '0' ORDER BY order_number";
                $result = mysqli_query($conn, $sql);

                while ($row = mysqli_fetch_assoc($result)) {

                    echo '
                    <li class="nav-item">
                        <a class="nav-link" href="?page=' . $row['id'] . '">' . $row['name'] . '</a>';

                    $sql2 = "SELECT * FROM cms_navigation WHERE parent_id = '" . $row['id'] . "' ORDER BY order_number";
                    $result2 = mysqli_query($conn, $sql2);

                    if (mysqli_num_rows($result2) > 0) {

                        echo '<ul>';

                        while ($row2 = mysqli_fetch_assoc($result2)) {

                            echo '
                            <li class="nav-item">
                                <a class="nav-link" href="?page=' . $row2['id'] . '">' . $row2['name'] . '</a>
                            </li>
                            ';

                        }

                        echo '</ul>';
                    }

                    echo '</li>';

                }
                ?>
            </ul>
        </div>
    </div>
</nav>