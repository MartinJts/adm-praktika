<?php
$conn = mysqli_connect($config['db_host'], $config['db_user'], $config['db_password'], $config['db_name']);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
};